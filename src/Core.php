<?php
declare(strict_types=1);

namespace Demodeos\Api;



use Demodeos\Api\Classes\ApiController;
use Demodeos\DB\Connection;
use Demodeos\Http\Cookies;
use Demodeos\Http\Headers;
use Demodeos\Http\Request;
use Demodeos\Http\Response;
use Demodeos\Http\Sessions;
use Demodeos\Router\Router;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SessionCookieJar;

class Core
{
    public static ?Core $app = null;
    private array|bool $router = ['controller'=>null, 'action'=>null, 'uri'=>null, 'params'=>null];
    private array       $config = [];
    private ?Response   $response = null;
    private ?Request    $request = null;
    private ?Cookies    $cookies = null;
    private ?Headers    $headers = null;
    private ?Sessions   $sessions = null;
    private ?Connection $sql = null;
    private Client $http;


    public function __construct($config)
    {

        $this->config = $config;
        self::$app = $this;

        $this->router();


        $this->run();
        //   MOX($this);

    }

    public static function init(array $config): static
    {

        static::$app ??= new static($config);
        return self::$app;
    }

    public function router():array|bool
    {
        $router_config = $this->config['router'];
        $router_class = new $router_config['classname']($router_config['params']['routes']);
        $this->router = $router_class->getRoute();
        return $this->router;

    }

    public function response(): Response
    {
        $this->response ??= new Response();
        return $this->response;
    }
    public function request(): Request
    {
        $this->request ??= new Request();
        return $this->request;
    }
    public function cookies(): Cookies
    {
        $this->cookies ??= new Cookies();
        return $this->cookies;
    }
    public function sessions(): Sessions
    {
        $this->sessions ??= new Sessions();
        return $this->sessions;
    }
    public function headers(): Headers
    {
        $this->headers ??= new Headers();
        return $this->headers;
    }
    public function sql(): Connection
    {
        if(isset ($this->config['sql']))
            $config = $this->config['sql'];
        else
            $config = null;
        $this->sql ??= new Connection($config);

        return $this->sql;
    }
    public function config(?string $name = null): array|false
    {
        if(is_null($name))
        {
            return $this->config;
        }
        else
        {
            if (isset($this->config[$name]))
                return $this->config[$name];
            else
                return false;
        }


    }
    public function http(string $service): Client|bool
    {
        if(isset($this->config('services')[$service]))
        {
            $service = $this->config('services')[$service];
            $client = new Client([
                'base_uri'=>$service,
                'cookies'=>true
            ]);
            return $client;
        }
        else
        {
            return false;
        }





    }
    public function json($service, $endpoint, $body, $method = 'POST', $clear_json = false)
    {

        $request = $this->http($service)->post($endpoint, ['body'=>$body]);

        $result = $request->getBody()->getContents();
        $data = json_decode($result);

    
        if(!$data)
            $data = $result;
        return $data;
    }

    public function run()
    {



        if($this->router!==false)
        {
            $controller = $this->router['controller'];
            $action = $this->router['action'];
            $params = $this->router['params'];
            /**
             * @var ApiController $controller_class
             */
            $controller_class = new $controller();

            if($controller_class->stop_execute === false)
                $controller_class->$action(...$params);

        }
        else
        {
            $controller = ApiController::class;

            (new $controller())->errorNotFound();

        }



    }

}