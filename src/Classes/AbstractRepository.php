<?php
declare(strict_types=1);

namespace Demodeos\Api\Classes;

use Demodeos\Api\Core;
use Demodeos\DB\Connection;

class AbstractRepository
{
    protected static  $_instance;
    protected Connection $_sql;

    public static function init(): static
    {
        self::$_instance = new static;
        self::$_instance->_sql = new Connection(Core::$app->config('sql'));



        return self::$_instance;

    }



}
