<?php
declare(strict_types=1);

namespace Demodeos\Api\Classes;

use Demodeos\Api\Core;

class ApiController
{
    public bool $stop_execute = false;

    public function __construct()
    {

        $this->beforeAction();


    }

    public function beforeAction()
    {

      //  echo 'beforeAction';

    }



    public function afterAction()
    {



    }

    protected function render(mixed $data, int $code = 200, bool $error = false)
    {
        $response = Core::$app->response();
        $response->setCode($code)->setBody($data)->setError($error)->emit();
    }

    public function __destruct()
    {
        $this->afterAction();
    }


    public function errorNotFound()
    {
        $response = Core::$app->response();
        $response->setCode(404)->setBody('Точка входа не найдена')->setError(true)->emit();


    }


}